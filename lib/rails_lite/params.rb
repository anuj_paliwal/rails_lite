require 'uri'

class Params
  attr_accessor :params

  def initialize(req, route_params)
    @req = req
    @params = parse_www_encoded_form(req.query_string)
    @req_body = parse_body(req.body)
    @params = @params.merge(@req_body)
  end

  def [](key)
    @params[key]
  end

  def to_s
  end


  def parse_www_encoded_form(www_encoded_form)
    p_hash = {}
    if www_encoded_form
      parsed_uri = URI::decode_www_form(www_encoded_form)
      parsed_uri.each {|key_val| p_hash[key_val[0]] = key_val[1]}
    end

    p_hash
  end

  def parse_body(req_body)
    parsed_hash = {}
    initial_parse = parse_www_encoded_form(req_body)
    puts "#{initial_parse}"
    initial_parse.each do |key, value|

      key_arr = key.split(/\]\[|\[|\]/)
      deepest_key = key_arr.pop
      parsed_hash = parsed_hash.deep_merge(parse_helper(key_arr, { deepest_key => value }))
      puts "parsed hash #{parsed_hash}"
      #puts "intermediate hash #{intermediate_hash}"
      #parsed_hash[key_arr[0]] = intermediate_hash[key_arr[0]]
      #parsed_hash = parsed_hash.merge(intermediate_hash)
    end

    parsed_hash
  end

  def parse_helper(arr, hash = {})
      #p arr
      return { arr[0] => hash } if arr.count <= 1
      key = arr.pop
      new_hash = { key => hash }
      parse_helper(arr, new_hash)
  end

end
