require 'json'
require 'webrick'

class Session
  def initialize(req)
    @cookies = {}
    @req = req
    @req.cookies.each do |cookie|
      @cookies = JSON.parse(cookie.value) if cookie.name == 'rails_lite_app'
    end
  end

  def [](key)
    @cookies[key]
  end

  def []=(key, val)
    @cookies[key] = val
  end

  def store_session(res)
    cookie = WEBrick::Cookie.new('rails_lite_app', @cookies.to_json)
    res.cookies << cookie
  end
end
