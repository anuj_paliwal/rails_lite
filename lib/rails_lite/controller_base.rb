require 'erb'
require_relative 'params'
require_relative 'session'
require 'active_support/core_ext'
require 'json'

class ControllerBase
  attr_reader :params
  attr_accessor :req, :res

  def initialize(req, res, route_params = {})
    @req = req
    @res = res
    @params = Params.new(req, route_params).params
  end

  def session
    @session ||= Session.new(@req)
  end

  def already_rendered?
  end

  def redirect_to(url)
    res.status = "302"
    res.header['location']= url.to_s
    @already_built_response = true
  end

  def render_content(content, type)
    session.store_session(@res)
    res.body = content
    res.content_type = type
    @already_built_response = true
  end

  def render(template_name)
    controller_name = "#{self.class.to_s}".underscore
    template_contents = File.read("views/#{controller_name}/#{template_name}.html.erb")
    template = ERB.new(template_contents).result(binding)
    render_content(template, 'text/text')
  end

  def invoke_action(name)
  end
end
